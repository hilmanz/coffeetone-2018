

/*-----------------------------------------------------------------------------------*/
/*		STICKY NAVIGATION
/*-----------------------------------------------------------------------------------*/
$(document).ready(function(){
    $(".sticky").sticky({topSpacing:0});
});


$('#open-button').click(function(){
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
 });

$(document).ready(function() {
'use strict';
	var owl = $("#owl-clients");
	  owl.owlCarousel({
		itemsCustom: [
		 [0, 1],
		 [450, 2],
		 [600, 3],
		 [700, 4],
		 [1024, 4],
		 [1200, 5],
		 [1400, 5],
		 [1600, 5],
		 [1920, 5]],
		navigation: true,
   	    pagination : false,
	    navigationText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"],
	});
});


$(document).ready(function () {
   'use strict';
    $('#submissionForm .error').remove();
    var form = $('#submissionForm'); // contact form
    var submit = $('#submissionForm_submit'); // submit button
    var alertx = $('.successMsg'); // alertx div for show alert message
    // form submit event
    form.on('submit', function (e) {
    var hasError = false;
      $('.required').each(function () {
          if (jQuery.trim($(this).val()) === '') {
              $(this).parent().append('<span class="error"><i class="fa fa-exclamation-triangle"></i></span>');
              hasError = true;
          } else if ($(this).hasClass('email')) {
              var emailReg = /^([\w-\.]+@([\w]+\.)+[\w]{2,4})?$/;
              if (!emailReg.test(jQuery.trim($(this).val()))) {
                  $(this).parent().append('<span class="error"><i class="fa fa-exclamation-triangle"></i></span>');
                  hasError = true;
              }
          }
      });
      if (!hasError) {
          e.preventDefault(); // prevent default form submit
        // sending ajax request through jQuery
        $.ajax({
            url: 'js/inc/sendemail.php', // form action url
            type: 'POST', // form submit method get/post
            dataType: 'html', // request type html/json/xml
            data: form.serialize(), // serialize form data
            beforeSend: function () {
                alertx.fadeOut();
                submit.html('Sending....'); // change submit button text
            },
            success: function (data) {
                form.fadeOut(300);
                alertx.html(data).fadeIn(1000); // fade in response data
                setTimeout(function() {
                  alertx.html(data).fadeOut(300);
                  $('#formName, #formEmail,#phone, #message').val('')
                  form.fadeIn(1800);
              }, 4000);
            },
            error: function (e) {
                console.log(e)
            }
        });
        $('.required').val('');
      }
      return false;
    });

  $('#submissionForm input').focus(function () {
      $('#submissionForm .error').remove();
  });
  $('#submissionForm textarea').focus(function () {
      $('#submissionForm .error').remove();
  });
});



/*-----------------------------------------------------------------------------------*/
/*  ISOTOPE PORTFOLIO
/*-----------------------------------------------------------------------------------*/
$(document).ready(function () {
    var $container = $('.portfolio-wrapper .items');
    $container.imagesLoaded(function () {
        $container.isotope({
            itemSelector: '.item',
            layoutMode: 'fitRows'
        });
  });
    $('.filter li a').click(function () {
        $('.filter li a').removeClass('active');
        $(this).addClass('active');
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector
        });
        return false;
    });
});

$(document).ready(function() {
'use strict';
 $("#owl-gallery").owlCarousel({
 	navigation : false, // Show next and prev buttons
	slideSpeed : 300,
	autoPlay:3000,
	pagination : false,
      singleItem:true,
	  items : 1
});
});


$(document).ready(function() {
'use strict';
  $("#owl-port").owlCarousel({
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
	  pagination : true,
	  items : 1,
	  navigationText: false
  });
});


//Mix it up
$(function() {
	'use strict';
    $('#portfolio-item').mixitup({
        targetSelector: '.item',
        transitionSpeed: 450
    });
});
$(function() {
	'use strict';
    $('#work_item').mixitup({
        targetSelector: ".item",
        filterSelector: ".filters",
        transitionSpeed: 450
    });
});

$(document).ready(function() {
  $("#owl-modal-gallery").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,
      items : 1,
      pagination : false,
	    navigationText: ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]

      // "singleItem:true" is a shortcut for:
      // items : 1,
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
  });

});
/*-----------------------------------------------------------------------------------*/
/* 	ANIMATION
/*-----------------------------------------------------------------------------------*/
var wow = new WOW({
    boxClass:     'wow',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       100,          // distance to the element when triggering the animation (default is 0)
    mobile:       false        // trigger animations on mobile devices (true is default)
});
wow.init();


$('.carousel').carousel({
  interval: 600000
})

var App = function() {
	// Handles Bootstrap Modals.
	var handleModals = function() {
			// fix stackable modal issue: when 2 or more modals opened, closing one of modal will remove .modal-open class.
			$('body').on('hide.bs.modal', function() {
					if ($('.modal:visible').size() > 1 && $('html').hasClass('modal-open') === false) {
							$('html').addClass('modal-open');
					} else if ($('.modal:visible').size() <= 1) {
							$('html').removeClass('modal-open');
					}
			});

			// fix page scrollbars issue
			$('body').on('show.bs.modal', '.modal', function() {
					if ($(this).hasClass("modal-scroll")) {
							$('body').addClass("modal-open-noscroll");
					}
			});

			// fix page scrollbars issue
			$('body').on('hide.bs.modal', '.modal', function() {
					$('body').removeClass("modal-open-noscroll");
			});

			// remove ajax content and remove cache on modal closed
			$('body').on('hidden.bs.modal', '.modal:not(.modal-cached)', function() {
					$(this).removeData('bs.modal');
			});
	};

	return {
		init: function() {
			handleModals(); // handle modals
		}
	}
}();


// EDIT POTO PROFILE //
$(document).ready(function() {
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $(".profile-pic-file-upload").on('change', function(){
        readURL(this);
    });

    $(".profile-pic-upload-button").on('click', function() {
       $(".profile-pic-file-upload").click();
    });
});

/*-----------------------------------------------------------------------------------*/
/* 	ADD Submission
/*-----------------------------------------------------------------------------------*/

$('#addsubmission-thumbs img').click(function(){
$('#addsubmission-largeImage').attr('src',$(this).attr('src').replace('thumb','large'));
});

/*-----------------------------------------------------------------------------------*/
/* 	Unite Gallery for Video Gallery
/*-----------------------------------------------------------------------------------*/
jQuery(document).ready(function(){

  jQuery("#video-gallery").unitegallery({
    gallery_width: "100%",
    grid_num_rows:4,
    tile_width: 275,						//tile width
		tile_height: 275,						//tile height

    theme_gallery_padding: 5,				//padding from sides of the gallery
    grid_padding:0,						//set padding to the grid
    grid_space_between_cols: 2,			//space between columns
		grid_space_between_rows: 2,			//space between rows

    tile_enable_border:false,
    tile_enable_shadow:false,

    tile_enable_textpanel: true,		 	//enable textpanel
		tile_textpanel_source: "title",   ////title,desc,desc_title. source of the textpanel. desc_title - if description empty, put title
    tile_textpanel_always_on: true,	 	//textpanel always visible
		//tile_textpanel_appear_type: "slide", 	//slide, fade - appear type of the textpanel on mouseover
		tile_textpanel_position:"inside_center", //inside_bottom, inside_top, inside_center, top, bottom the position of the textpanel
		tile_textpanel_offset:"60",			    //vertical offset of the textpanel

    tile_textpanel_title_color:null,		 //textpanel title color. if null - take from css
    tile_textpanel_title_font_family:"Lato, sans-serif",	 //textpanel title font family. if null - take from css
    tile_textpanel_title_text_align:"center",	 //textpanel title text align. if null - take from css
    tile_textpanel_title_font_size:16,	 //textpanel title font size. if null - take from css
    tile_textpanel_title_bold:null,			 //textpanel title bold. if null - take from css
    tile_textpanel_css_title:{},			 //textpanel additional css of the title
    tile_textpanel_bg_opacity: 0,

    lightbox_type: "compact",
    lightbox_overlay_color:"#bf9d73",					//the color of the overlay. if null - will take from css
		lightbox_overlay_opacity:0.9,						//the opacity of the overlay. for compact type - 0.6
		lightbox_top_panel_opacity: null,				//the opacity of the top panel
    lightbox_show_numbers: false,
    lightbox_slider_image_border: false,
    lightbox_slider_image_shadow: false,
    lightbox_show_textpanel: false,
  });

});
